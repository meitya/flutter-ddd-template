import 'package:flutter/material.dart';
import 'package:flutter_template/injection.dart';
import 'package:flutter_template/presentation/core/app_widget.dart';
import 'package:injectable/injectable.dart';

void main() {
  configureInjection(Environment.prod);
  runApp(AppWidget());
}
