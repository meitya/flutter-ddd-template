import 'package:flutter/material.dart';
import 'package:flutter_template/presentation/sign_in/sign_in_page.dart';

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Template App',
      theme: ThemeData.light().copyWith(
        primaryColor: Colors.amber[600],
        accentColor: Colors.lightBlueAccent,
        inputDecorationTheme: InputDecorationTheme(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8),
          ),
        ),
      ),
      home: SignInPage(),
    );
  }
}
